Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  
  get '/reports/open' => 'reports#open'
  get '/reports/closed' => 'reports#closed'
  resources :reports
end
