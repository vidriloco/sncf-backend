class Report < ApplicationRecord
  validates :message, presence: true
  
  before_create :assign_random_reporter_info
  
  rails_admin do
  end
  
  def report_kind_enum
    ['broken', 'bad-smell', 'temperature', 'noisy', 'other']
  end
  
  def reporter_enum
    ['Lady Gagus', 'Tim Cooking', 'Thomas Rutte']
  end
  
  def reporter_img_slug_enum
    ['lady-gagus', 'tim-cooking', 'thomas-rutte']
  end
  
  def assign_random_reporter_info
    random_number = rand(0..2)
    self.reporter_img_slug = reporter_img_slug_enum[random_number]
    self.reporter = reporter_enum[random_number]
    self.open = true
  end
end
