class ReportsController < ApplicationController
  
  def index
    json_response(Report.all)
  end
  
  def open
    json_response(Report.where(open: true))
  end
  
  def closed
    json_response(Report.where(open: false))
  end
  
  def show
    @report = Report.find(params[:id])
    json_response(@report)
  end
  
  def create
    @report = Report.new(reports_params)
    if @report.save
      json_response(@report, :created)
    else
      render json: {}, status: 400
    end
  end
  
  private
  def reports_params
    params.require(:report).permit(:report_kind, :message)
  end
end
