class CreateReports < ActiveRecord::Migration[5.1]
  def change
    create_table :reports do |t|
      t.string    :report_kind
      t.text      :message
      t.boolean   :open
      t.string    :reporter
      t.string    :reporter_img_slug

      t.timestamps
    end
  end
end
